import org.junit.Before;
import org.junit.Test;
import utils.ParsedDocumentQuery;

import java.nio.file.Path;
import java.nio.file.Paths;

@SuppressWarnings("ALL")
public class AppTest {

    private Path testPath;

    @Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        testPath = Paths.get(classLoader.getResource("test.txt").getPath());
    }

    @Test
    public void runQueryTest() throws Exception {
        ParsedDocumentQuery query = new ParsedDocumentQuery(testPath, true, null, null, 3, 1);
        App.runQuery(query);
    }

}