package domain;

import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("ALL")
public class DocumentTest {

    private Document document;

    @Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        Path testPath = Paths.get(classLoader.getResource("test.txt").getPath());
        document = Document.fromPath(testPath);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fromPathShouldThrowAnExceptionIfTheFilePathIsNotValid() throws Exception {
        Document.fromPath(Paths.get("wrongPath"));
    }

    @Test
    public void articleShouldReturnTheFirstArticleInTheFirstChapter() throws Exception {
        assertEquals("Art. 1. \nRzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli. ", document.article(1).toString());
    }

    @Test
    public void articleShouldReturnTheFirstArticleInAnotherChapter() throws Exception {
        assertEquals("Art. 30. \nPrzyrodzona i niezbywalna godność człowieka stanowi źródło wolności i praw człowieka i obywatela. Jest ona nienaruszalna, a jej poszanowanie i ochrona jest obowiązkiem władz publicznych. ", document.article(30).toString());
    }

    @Test
    public void articleShouldReturnTheLastArticleInTheLastChapter() throws Exception {
        assertEquals("Art. 243. \nKonstytucja Rzeczypospolitej Polskiej wchodzi w życie po upływie 3 miesięcy od dnia jej ogłoszenia. ", document.article(243).toString());
    }

    @Test
    public void theReturnedListOfArticlesFrom1To29ShouldHaveAppropriateSize() throws Exception {
        assertEquals(29, document.articles(1, 29).size());
    }

    @Test
    public void theReturnedListOfArticlesFrom1To30ShouldHaveAppropriateSize() throws Exception {
        assertEquals(30, document.articles(1, 30).size());
    }

    @Test
    public void theReturnedListOfAllArticlesShouldHaveAppropriateSize() throws Exception {
        assertEquals(243, document.articles(1, 243).size());
    }

}