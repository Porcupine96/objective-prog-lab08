package utils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


@SuppressWarnings("ALL")
public class ArgumentParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Path testPath;

    @Before
    public void setUp() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        testPath = Paths.get(classLoader.getResource("test.txt").getPath());
    }

    @Test
    public void argumentParserShouldReturnSingleArticleQuery() throws Exception {
        String[] args = {"-F", testPath.toString(), "-A", "1", "2", "-a", "1"};
        ParsedDocumentQuery query = ArgumentParser.parse(args);

        assertEquals(testPath, query.filePath());
        assertEquals(1, query.articleLowerBound().get().intValue());
        assertEquals(2, query.articleUpperBound().get().intValue());
        assertEquals(1, query.articleNumber().get().intValue());
        assertFalse(query.chapterNumber().isPresent());
    }

    @Test
    public void argumentParserShouldThrowParseExceptionWhenOneOfTheParametersIsNotANumber() throws Exception {
        thrown.expect(InvalidArgumentsFormatException.class);
        thrown.expectMessage(is("Could not parse 1a1 as an integer value"));
        thrown.expectCause(isA(NumberFormatException.class));

        String[] args = {"-F", testPath.toString(), "-c", "1a1"};
        ArgumentParser.parse(args);
    }

}