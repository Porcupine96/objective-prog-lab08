import domain.Document;
import utils.ArgumentParser;
import utils.ParsedDocumentQuery;

public class App {

    public static void main(String[] args) {
        runQuery(ArgumentParser.parse(args));
    }

    static void runQuery(ParsedDocumentQuery query) {

        try {

            Document document = Document.fromPath(query.filePath());

            if (query.displayIntroduction()) System.out.println(document.introduction());
            System.out.println("\n\n");
            query.chapterNumber().ifPresent(c -> System.out.println(document.chapter(c)));
            System.out.println("\n\n");
            query.articleNumber().ifPresent(a -> System.out.println(document.article(a)));
            System.out.println("\n\n");
            query.articleLowerBound()
                    .ifPresent(l -> query.articleUpperBound()
                            .ifPresent(u ->
                                    document.articles(l, u)
                                            .forEach(System.out::println)));

        } catch (Exception ex) {
            System.out.println("[ERROR]: " + ex.getMessage());
        }

    }

}
