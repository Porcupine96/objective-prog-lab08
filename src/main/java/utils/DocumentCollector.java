package utils;

import domain.Document;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class DocumentCollector implements Collector<String, DocumentSupplier, Document> {

    @Override
    public Supplier<DocumentSupplier> supplier() {
        return DocumentSupplier::new;
    }

    @Override
    public BiConsumer<DocumentSupplier, String> accumulator() {
        return DocumentSupplier::add;
    }

    @Override
    public BinaryOperator<DocumentSupplier> combiner() {
        return null;
    }

    @Override
    public Function<DocumentSupplier, Document> finisher() {
        return DocumentSupplier::get;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return new HashSet<>();
    }

}

