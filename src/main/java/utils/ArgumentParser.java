package utils;

import org.apache.commons.cli.*;

import java.nio.file.Paths;
import java.util.Optional;

public class ArgumentParser {

    public static ParsedDocumentQuery parse(String[] args) {
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(setUpParsersOptions(), args);
            String[] boundaries = cmd.getOptionValues("A");
            if (boundaries == null) boundaries = new String[2];

            return new ParsedDocumentQuery(
                    Paths.get(cmd.getOptionValue("F")),
                    cmd.hasOption("i"),
                    extractIntegerParam(cmd.getOptionValue("c")),
                    extractIntegerParam(cmd.getOptionValue("a")),
                    extractIntegerParam(boundaries[0]),
                    extractIntegerParam(boundaries[1])
            );
        } catch (ParseException ex) {
            throw new InvalidArgumentsFormatException("Illegal option \n usage: [-F & -a | -A | -c] ", ex);
        }
    }

    private static Options setUpParsersOptions() {

        Options options = new Options();

        Option filePath =
                Option
                        .builder("F")
                        .desc("a file path to the document directory")
                        .hasArg()
                        .required()
                        .build();

        Option chapterInterval =
                Option
                        .builder("A")
                        .desc("an interval of article numbers")
                        .hasArg()
                        .numberOfArgs(2)
                        .build();

        options.addOption(filePath);
        options.addOption(chapterInterval);
        options.addOption("a", true, "an article number");
        options.addOption("c", true, "a chapter number");
        options.addOption("i", false, "the document introduction");

        return options;
    }

    private static Integer extractIntegerParam(String arg) {
        try {
            return Optional.ofNullable(arg).map(Integer::valueOf).orElse(null);
        } catch (NumberFormatException ex) {
            throw new InvalidArgumentsFormatException("Could not parse " + arg + " as an integer value", ex);
        }
    }

}

