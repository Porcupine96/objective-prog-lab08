package utils;

import domain.Document;
import domain.entity.Article;
import domain.entity.Chapter;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;


class DocumentSupplier implements Supplier<Document> {

    private StringBuilder introductionBuilder = new StringBuilder();
    private StringBuilder chapterBuilder = new StringBuilder();
    private StringBuilder articleBuilder = new StringBuilder();
    private List<Article> articles = new LinkedList<>();
    private List<Chapter> chapters = new LinkedList<>();

    DocumentSupplier add(String s) {
        if (s.startsWith("Rozdział ")) {
            if (chapterBuilder.length() > 0) finishLastChapter();
            appendPlainLine(s, chapterBuilder);
        } else if (s.startsWith("Art. ")) {
            if (articleBuilder.length() > 0) finishLastArticle();
            appendPlainLine(s, articleBuilder);
            articleBuilder.append("\n");
        } else {
            if (chapterBuilder.length() == 0 && articleBuilder.length() == 0) appendPlainLine(s, introductionBuilder);
            else if (articleBuilder.length() == 0) appendPlainLine(s, chapterBuilder);
            else appendPlainLine(s, articleBuilder);
        }
        return this;
    }

    private void appendPlainLine(String s, StringBuilder builder) {
        if (s.matches("^[2-9]+\\. .*")) builder.append("\n");
        if (s.matches("^[1-9]+\\) .*")) builder.append("\n\t");
        if (s.endsWith("-")) builder.append(s.substring(0, s.length() - 1));
        else if (s.matches("^[A-ZĄĘŚŹŁÓŻĆŃ, ]+")) builder.append("\n").append(s);
        else builder.append(s).append(" ");
    }

    private void finishLastArticle() {
        articles.add(new Article(articleBuilder.toString()));
        articleBuilder.setLength(0);
    }

    private void finishLastChapter() {
        finishLastArticle();
        chapters.add(new Chapter(chapterBuilder.toString(), articles));
        chapterBuilder.setLength(0);
        articles = new LinkedList<>();
    }

    @Override
    public Document get() {
        finishLastChapter();
        return new Document(introductionBuilder.toString(), chapters);
    }

}
