package utils;

class InvalidArgumentsFormatException extends RuntimeException {

    InvalidArgumentsFormatException() {

    }

    InvalidArgumentsFormatException(String message) {
        super(message);
    }

    InvalidArgumentsFormatException(String message, Throwable cause) {
        super(message, cause);
    }

}
