package utils;

import com.sun.istack.internal.Nullable;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.nio.file.Path;
import java.util.Optional;


@AllArgsConstructor
public class ParsedDocumentQuery {

    @NonNull
    private Path filePath;

    @NonNull
    private Boolean introduction;

    @Nullable
    private Integer chapterNumber;

    @Nullable
    private Integer articleNumber;

    @Nullable
    private Integer articleLowerBound;

    @Nullable
    private Integer articleUpperBound;

    public Path filePath() {
        return filePath;
    }

    public Boolean displayIntroduction() {
        return introduction;
    }

    public Optional<Integer> chapterNumber() {
        return Optional.ofNullable(chapterNumber);
    }

    public Optional<Integer> articleNumber() {
        return Optional.ofNullable(articleNumber);
    }

    public Optional<Integer> articleLowerBound() {
        return Optional.ofNullable(articleLowerBound);
    }

    public Optional<Integer> articleUpperBound() {
        return Optional.ofNullable(articleUpperBound);
    }

}


