package domain;

import utils.DocumentCollector;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

class DocumentParser {

    static Document parse(Path filePath) throws IOException {
        return Files
                .lines(filePath)
                .filter(ignoreUnnecessaryElements())
                .collect(new DocumentCollector());
    }

    private static Predicate<String> ignoreUnnecessaryElements() {
        return ((Predicate<String>)
                (s -> !s.startsWith("©Kancelaria Sejmu")))
                .and(s -> !s.matches("^\\d{4}-\\d{2}-\\d{2}$"))
                .and(s -> s.length() > 1);
    }

}

