package domain.entity;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@EqualsAndHashCode
public class Chapter {

    private String header;
    private List<Article> articles;

    public List<Article> articles(int lowerBound, int upperBound) {
        return Collections.unmodifiableList(articles.subList(lowerBound, upperBound));
    }

    public Integer numberOfArticles() {
        return articles.size();
    }

    @Override
    public String toString() {
        return header + "\n" + articles
                .stream()
                .map(Article::toString)
                .collect(Collectors.joining("\n"));
    }

}
