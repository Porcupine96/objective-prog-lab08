package domain.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@EqualsAndHashCode
public class Article {

    private String content;

    @Override
    public String toString() {
        return content;
    }

}
