package domain;

import com.sun.tools.javac.util.ListBuffer;
import domain.entity.Article;
import domain.entity.Chapter;
import lombok.AllArgsConstructor;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class Document {

    private String introduction;
    private List<Chapter> chapters;

    public static Document fromPath(Path filePath) {
        try {
            return DocumentParser.parse(filePath);
        } catch (IOException ex) {
            throw new IllegalArgumentException("Invalid file path!", ex);
        }
    }

    public String introduction() {
        return introduction;
    }

    public Chapter chapter(Integer chapterNumber) {
        try {
            return chapters.get(chapterNumber - 1);
        } catch (IndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("Invalid chapter number!", ex);
        }
    }

    public Article article(int articleNumber) {
        return articles(articleNumber, articleNumber).get(0);
    }

    public List<Article> articles(int lowerBound, int upperBound) {

        validateBound(lowerBound, upperBound);

        int currentArticleNumber = 0;
        int articlesGathered = 0;

        ListBuffer<Article> buffer = new ListBuffer<>();

        for (Chapter chapter : chapters) {
            int numberOfArticles = chapter.numberOfArticles();
            if (articlesGathered == upperBound - lowerBound + 1) break;
            else if (currentArticleNumber + numberOfArticles < lowerBound) currentArticleNumber += numberOfArticles;
            else {
                int scaledLowerBound = Math.max(lowerBound - currentArticleNumber - 1, 0);
                int scaledUpperBound = Math.min(upperBound - currentArticleNumber, numberOfArticles);
                buffer.addAll(chapter.articles(scaledLowerBound, scaledUpperBound));
                articlesGathered += (scaledUpperBound - scaledLowerBound);
                currentArticleNumber += numberOfArticles;
            }
        }

        return buffer.toList();
    }

    private void validateBound(int lowerBound, int upperBound) {
        if (lowerBound < 0 || upperBound < 0 || upperBound > numberOfArticles() || lowerBound > upperBound)
            throw new IllegalArgumentException("Invalid article boundaries!");
    }

    private int numberOfArticles() {
        return chapters.stream().mapToInt(Chapter::numberOfArticles).sum();
    }

    @Override
    public String toString() {
        return introduction + "\n" +
                chapters
                        .stream()
                        .map(Chapter::toString)
                        .collect(Collectors.joining("\n"));
    }

}


